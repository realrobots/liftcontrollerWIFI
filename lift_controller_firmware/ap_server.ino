#include <HTTPClient.h>
#include <WebServer.h>
const char *APName = "RealRobotsLift";

int i = 0;
int statusCode;
String st;
String content;
String esid;
String epass = "";
WebServer server(80);

String GetAPName()
{
  return APName;
}

void GetCredsFromEEPROM() {
  for (int i = 0; i < 64; i++) {
    WIFI_SSID[i] = char(Read8BitValue(64 + 0 + i));
  }
  for (int i = 0; i < 64; i++) {
    WIFI_PASS[i] = char(Read8BitValue(64 + 64 + i));
  }
}


void InitWifi()
{
  GetCredsFromEEPROM();
  Serial.print("Connecting to WiFi: ");
  //Serial.print("SSID\t");
  Serial.println(WIFI_SSID);
  WiFi.begin(WIFI_SSID, WIFI_PASS);

  int failCount = 0;
  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.printf(".");
    delay(250);
    failCount++;
    if (failCount >= 20)
    {
      Serial.println("Wifi Connection Failed");
      break;
    }
  }
  if (WiFi.status() == WL_CONNECTED)
  {
    Serial.printf("connected!\r\n[WiFi]: IP-Address is %s\r\n", WiFi.localIP().toString().c_str());
  }
}


void launchWeb()
{
  Serial.println("");
  if (WiFi.status() == WL_CONNECTED)
    Serial.println("WiFi connected");
  Serial.print("Local IP: ");
  Serial.println(WiFi.localIP());
  Serial.print("SoftAP IP: ");
  Serial.println(WiFi.softAPIP());
  createWebServer();
  // Start the server
  server.begin();
  Serial.println("Server started");
}

void setupAP(void)
{
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  SetIndicatorFlashCount(5);
  delay(100);
  int n = WiFi.scanNetworks();
  Serial.println("scan done");
  if (n == 0)
    Serial.println("no networks found");
  else
  {
    Serial.print(n);
    Serial.println(" networks found");
    for (int i = 0; i < n; ++i)
    {
      // Print SSID and RSSI for each network found
      Serial.print(i + 1);
      Serial.print(": ");
      Serial.print(WiFi.SSID(i));
      Serial.print(" (");
      Serial.print(WiFi.RSSI(i));
      Serial.print(")");
      // Serial.println((WiFi.encryptionType(i) == ENC_TYPE_NONE) ? " " : "*");
      delay(10);
    }
  }
  Serial.println("");
  st = "<ol>";
  for (int i = 0; i < n; ++i)
  {
    // Print SSID and RSSI for each network found
    st += "<li>";
    st += WiFi.SSID(i);
    st += " (";
    st += WiFi.RSSI(i);

    st += ")";
    // st += (WiFi.encryptionType(i) == ENC_TYPE_NONE) ? " " : "*";
    st += "</li>";
  }
  st += "</ol>";
  delay(100);
  WiFi.softAP(APName, "");
  Serial.println("Initializing_softap_for_wifi credentials_modification");
  launchWeb();
}

void createWebServer()
{
  {
    server.on("/", []()
    {

      IPAddress ip = WiFi.softAPIP();
      String ipStr = String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]);
      content = "<!DOCTYPE HTML>\r\n<html>Welcome to Wifi Credentials Update page";
      content += "<p>Device MAC address: " + WiFi.macAddress() + "</p>";
      content += "<p>Use this address to register your device to your google account at the link below (after wifi is connected)</p>";
      content += "<a href=\"https://us-central1-reallift-502df.cloudfunctions.net/login\">https://us-central1-reallift-502df.cloudfunctions.net/login</a>";
      content += "</br>";
      content += "<form action=\"/scan\" method=\"POST\"><input type=\"submit\" value=\"scan\"></form>";
      content += ipStr;
      content += "<p>";
      content += st;
      content += "</p><form method='get' action='setting'><label>SSID: </label><input name='ssid' length=32><input name='pass' length=64><input type='submit'></form>";
      content += "</html>";
      server.send(200, "text/html", content);
    });
    server.on("/scan", []()
    {
      //setupAP();
      IPAddress ip = WiFi.softAPIP();
      String ipStr = String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]);

      content = "<!DOCTYPE HTML>\r\n<html>go back";
      server.send(200, "text/html", content);
    });

    server.on("/setting", []()
    {

      String qsid = server.arg("ssid");
      String qpass = server.arg("pass");
      if (qsid.length() > 0 && qpass.length() > 0)
      {
        Serial.println("clearing eeprom");
        for (int i = 0; i < 128; ++i)
        {
          EEPROM.write(i + 64, 0);
        }
        Serial.println(qsid);
        Serial.println("");
        Serial.println(qpass);
        Serial.println("");

        Serial.println("writing eeprom ssid:");
        for (int i = 0; i < qsid.length(); ++i)
        {
          Store8BitValue(i+64, qsid[i]);
          //EEPROM.write(i + 64, qsid[i]);
          Serial.print("Wrote: ");
          Serial.print(i + 64);
          Serial.println(qsid[i]);
        }
        Store8BitValue(qsid.length()+64, '\0');
        Serial.println("writing eeprom pass:");
        for (int i = 0; i < qpass.length(); ++i)
        {
          Store8BitValue(64 + 64 + i, qpass[i]);
         // EEPROM.write(64 + 64 + i, qpass[i]);
          Serial.print("Wrote: ");
          Serial.print(64 + 64 + i);
          Serial.println(qpass[i]);
        }
         Store8BitValue(qpass.length()+64 + 64, '\0');
//        EEPROM.commit();
//        EEPROM.end();
        content = "{\"Success\":\"saved to eeprom... resetting to boot into new wifi\"}";
        statusCode = 200;
        //ESP.restart();
        esid = qsid;
        epass = qpass;
        WiFi.disconnect();
        GetCredsFromEEPROM();
        Serial.print("SSID\t");
  Serial.println(WIFI_SSID);
        delay(50);
        ESP.restart();
      }
      else
      {
        content = "{\"Error\":\"404 not found\"}";
        statusCode = 404;
        Serial.println("Sending 404");
      }
      server.sendHeader("Access-Control-Allow-Origin", "*");
      server.send(statusCode, "application/json", content);
    });
  }
}

void ServerLoop()
{
  server.handleClient();
}
