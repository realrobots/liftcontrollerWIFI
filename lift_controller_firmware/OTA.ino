#include <HTTPClient.h>

#include <Arduino_JSON.h>
#include <Update.h>

HTTPClient http;
int totalLength;       // total size of firmware
int currentLength = 0; // current size of written firmware

void CheckForUpdate()
{
  Serial.println("Connecting to update server...");
  http.begin("https://www.realrobots.net/files/ota/liftcontroller_version.json"); // HTTP

  // start connection and send HTTP header
  int httpCode = http.GET();

  if (httpCode > 0)
  {

    // file found at server
    if (httpCode == HTTP_CODE_OK)
    {
      bool fileValid = true;
      String payload = http.getString();

      JSONVar myObject = JSON.parse(payload);
      if (JSON.typeof(myObject) == "undefined")
      {
        Serial.println("Parsing input failed!");
        return;
      }

      // Serial.print("JSON.typeof(myObject) = ");
      // Serial.println(JSON.typeof(myObject)); // prints: object

      // myObject.hasOwnProperty(key) checks if the object contains an entry for key
      if (myObject.hasOwnProperty("latestVersion"))
      {
        // Serial.print("myObject[\"latestVersion\"] = ");

        // Serial.println((int)myObject["latestVersion"]);
      }
      else
      {
        fileValid = false;
      }

      if (myObject.hasOwnProperty("latestBinary"))
      {
        // Serial.print("myObject[\"latestBinary\"] = ");

        // Serial.println((const char *)myObject["latestBinary"]);
      }
      else
      {
        fileValid = false;
      }

      if (fileValid)
      {
        if ((int)myObject["latestVersion"] > CURRENT_VERSION)
        {
          Serial.println("New version available");
          Serial.println((int)myObject["latestVersion"]);
          Serial.println((const char *)myObject["latestBinary"]);
          InitOTA((const char *)myObject["latestBinary"]);
        }
        else
        {
          Serial.println("Current version up to date.");          
          SetBuzzerActive(1);
          delay(200);
          SetBuzzerActive(0);
          delay(100);
        }
      }
    }
  }
  else
  {
    Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
  }

  http.end();
}

void InitOTA(const char *updateLink)
{
  for (int i = 0; i < 6; i++) {
    SetBuzzerActive(1);
    delay(100);
    SetBuzzerActive(0);
    delay(100);
  }
  SetBuzzerActive(1);
  delay(200);
  SetBuzzerActive(0);
  delay(100);

  // Connect to external web server
  http.begin(updateLink);
  // Get file, just to check if each reachable
  int resp = http.GET();
  Serial.print("Response: ");
  Serial.println(resp);
  // If file is reachable, start downloading
  if (resp == 200)
  {
    // get length of document (is -1 when Server sends no Content-Length header)
    totalLength = http.getSize();
    // transfer to local variable
    int len = totalLength;
    // this is required to start firmware update process
    Update.begin(UPDATE_SIZE_UNKNOWN);
    Serial.printf("FW Size: %u\n", totalLength);
    // create buffer for read
    uint8_t buff[128] = {0};
    // get tcp stream
    WiFiClient *stream = http.getStreamPtr();
    // read all data from server
    Serial.println("Updating firmware...");
    while (http.connected() && (len > 0 || len == -1))
    {
      // get available data size
      size_t size = stream->available();
      if (size)
      {
        // read up to 128 byte
        int c = stream->readBytes(buff, ((size > sizeof(buff)) ? sizeof(buff) : size));
        // pass to function
        updateFirmware(buff, c);
        if (len > 0)
        {
          len -= c;
        }
      }
      delay(1);
    }
  }
  else
  {
    Serial.println("Cannot download firmware file. Only HTTP response 200: OK is supported. Double check firmware location #defined in HOST.");
  }
  http.end();
}

// Function to update firmware incrementally
// Buffer is declared to be 128 so chunks of 128 bytes
// from firmware is written to device until server closes
void updateFirmware(uint8_t *data, size_t len)
{

  Update.write(data, len);
  currentLength += len;
  // Print dots while waiting for update to finish
  Serial.println(currentLength);
  // if current length of written firmware is not equal to total firmware size, repeat
  if (currentLength != totalLength)
    return;
  Update.end(true);
  Serial.printf("\nUpdate Success, Total Size: %u\nRebooting...\n", currentLength);
  // Restart ESP32 to see changes
  ESP.restart();
}
