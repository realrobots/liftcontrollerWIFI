#include <Wire.h>
#include "Adafruit_AW9523.h"
#include <EEPROM.h>
#include <WiFi.h>
#define EEPROM_SIZE 1024

#define CURRENT_VERSION 3
String deviceType = "LiftControllerESP32";
char WIFI_SSID[64] = "";
char WIFI_PASS[64] = "";

bool wifiEnabled = false;

#define PIN_STATUS_LED 2

#define MCP_PIN_BUZZER 6 // buzzer on mcp
#define MCP_PIN_LATCH 7

#define MCP_INPUT_ESTOP 4
#define MCP_INPUT_UP 2
#define MCP_INPUT_DOWN 3
#define MCP_INPUT_ALARM 5

#define MCP_INPUT_GATE 1
uint8_t gateIsClosed = false;
#define MCP_INPUT_LIFT_MIN 10
#define MCP_INPUT_LIFT_MAX 9
#define MCP_INPUT_RAMP_MIN 8
#define MCP_INPUT_RAMP_MAX 0

#define MOTOR1_INH_MCP 11
#define MOTOR1_IN1 14
#define MOTOR1_IN2 33

#define MOTOR2_INH_MCP 12
#define MOTOR2_IN1 32
#define MOTOR2_IN2 4

int motor1_speed = 0; //-255 full reverse, 0 stop, 255 full forward
int motor1_target = 0;
int motor2_speed = 0; //-255 full reverse, 0 stop, 255 full forward
int motor2_target = 0;

Adafruit_AW9523 mcp;

long subtaskStartTime = 0;
int subtaskTimeout = 30000; // 30 second timeout for every action

#define FLASH_IDLE 1
#define FLASH_MOVING_UP 2
#define FLASH_MOVING_DOWN 3
#define FLASH_ALARM_PRESSED 5
#define FLASH_OVERRIDE_MODE 7

int flashCount = 1;
int currentFlashCount = 0;
long lastFlash = 0;
int longFlashInterval = 3000;
int shortFlashInterval = 300;

#define MODE_NORMAL 0        // Normal mode, ready for button presses and standard tasks
#define MODE_TEST 1          // In test mode, buttons don't trigger tasks
#define MODE_ALARM_PRESSED 2 // Estop pressed, hold estop for override, any other button switches back to normal
#define MODE_OVERRIDE 3      // Tasks disabled, gate unlocked, up/down directly controls lift motors

int currentMode = MODE_NORMAL;

// If alarm held longer than interval, go to override mode (direct up/down control, tasks disabled)
bool alarmDown = false;
long alarmDownTime = 0;
int alarmOverrideInterval = 10000;

int temp = 0;

bool liftMaxState = 0;
bool liftMinState = 0;
bool rampMaxState = 0;
bool rampMinState = 0;
bool gateSenseState = 0;

long liftMaxChangeStart = 0;
long liftMinChangeStart = 0;
long rampMaxChangeStart = 0;
long rampMinChangeStart = 0;
long gateSenseChangeStart = 0;

long senseDelayFilter = 250;

void setup()
{
  InitComms();
  Serial.println("Serial Communications Activated");
  LoadSettings();
  Serial.println("Loaded saved settings");
  mcp.begin(); // use default address 0

  pinMode(PIN_STATUS_LED, OUTPUT);

  pinMode(MOTOR1_IN1, OUTPUT);
  pinMode(MOTOR1_IN2, OUTPUT);
  mcp.pinMode(MOTOR1_INH_MCP, OUTPUT);
  mcp.digitalWrite(MOTOR1_INH_MCP, LOW);

  pinMode(MOTOR2_IN1, OUTPUT);
  pinMode(MOTOR2_IN2, OUTPUT);
  mcp.pinMode(MOTOR2_INH_MCP, OUTPUT);
  mcp.digitalWrite(MOTOR2_INH_MCP, LOW);

  mcp.pinMode(MCP_PIN_BUZZER, OUTPUT);
  mcp.pinMode(MCP_PIN_LATCH, OUTPUT);
  SetLatchOpen(true);

  mcp.pinMode(MCP_INPUT_ESTOP, INPUT);
  mcp.pinMode(MCP_INPUT_UP, INPUT);
  mcp.pinMode(MCP_INPUT_DOWN, INPUT);
  mcp.pinMode(MCP_INPUT_ALARM, INPUT);
  mcp.pinMode(MCP_INPUT_GATE, INPUT);
  mcp.pinMode(MCP_INPUT_LIFT_MIN, INPUT);
  mcp.pinMode(MCP_INPUT_LIFT_MAX, INPUT);
  mcp.pinMode(MCP_INPUT_RAMP_MIN, INPUT);
  mcp.pinMode(MCP_INPUT_RAMP_MAX, INPUT);

  for (int i = 0; i < 3; i++)
  {
    SetBuzzerActive(1);
    delay(100);
    SetBuzzerActive(0);
    delay(100);
  }
  SetBuzzerActive(1);
  delay(200);
  SetBuzzerActive(0);
  delay(100);

  // SetCurrentMode(MODE_OVERRIDE);
  if (!mcp.digitalRead(MCP_INPUT_ALARM))
  {
    wifiEnabled = true;
    Serial.println("WIFI enabled");
    InitWifi();
    if (WiFi.status() == WL_CONNECTED)
    {
      // InitFirebase();
      Serial.println("Wifi Connected");
      CheckForUpdate();
    }
    else
    {
      Serial.print("Initializing Access Point ");
      Serial.print(GetAPName());
      Serial.println(" for wifi credential entry...");
      // launchWeb();
      setupAP(); // Setup HotSpot
    }
  }
  else
  {
    Serial.println("Initialized in normal mode");
  }

  // Motor1SlowDrive(20);
}

void loop()
{
  CheckComms();

  FlashIndicator();

  if (currentMode == MODE_NORMAL || currentMode == MODE_ALARM_PRESSED)
  {
    CheckInputs();
    CheckAlarm();
    UpdateMotorControl();

    DoTasks();
  }
  else if (currentMode == MODE_OVERRIDE)
  {
    CheckAlarm();
    //  Serial.println("override");
    if (ButtonStateEstop())
    {
      if (ButtonStateINPUT_UP())
      {
        SetMotorSpeed(1, 255);
        alarmDownTime = millis();
      }
      else if (ButtonStateINPUT_DOWN())
      {
        SetMotorSpeed(1, -255);
        alarmDownTime = millis();
      }
      else
      {
        SetMotorSpeed(1, 0);
      }
    }
    else
    {
      if (ButtonStateINPUT_UP())
      {
        SetMotorSpeed(2, 255);
      }
      else if (ButtonStateINPUT_DOWN())
      {
        SetMotorSpeed(2, -255);
      }
      else
      {
        SetMotorSpeed(2, 0);
      }
    }
  }

  if (wifiEnabled)
  {
    if (WiFi.status() == WL_CONNECTED)
    {
    }
    else
    {
      ServerLoop();
    }
  }

  // Update Button States
  ButtonStateRAMP_MAX();
  ButtonStateRAMP_MIN();
  ButtonStateLIFT_MAX();
  ButtonStateLIFT_MIN();
  ButtonStateGATE();

  delay(10);
}

void SetCurrentMode(int newMode)
{
  currentMode = newMode;

  // Set indicator flash count
  switch (currentMode)
  {
  case MODE_NORMAL:
    SetIndicatorFlashCount(FLASH_IDLE);
    break;
  case MODE_ALARM_PRESSED:
    SetIndicatorFlashCount(FLASH_ALARM_PRESSED);
    break;
  case MODE_OVERRIDE:
    SetIndicatorFlashCount(FLASH_OVERRIDE_MODE);
    break;
  }
  Serial.print("Mode: ");
  Serial.println(currentMode);
}

void CheckInputs()
{
  // if up or down button pushed and not already doing so or in that position, begin the action
  if (ButtonStateINPUT_UP())
  {
    if (!IsGoingUp() && !ButtonStateLIFT_MAX())
    {
      StartActionGoUp();
    }
  }
  if (ButtonStateINPUT_DOWN())
  {
    if (!IsGoingDown() && !ButtonStateLIFT_MIN())
    {
      StartActionGoDown();
    }
  }

  gateIsClosed = ButtonStateGATE();
  if (!gateIsClosed)
  {
    if (IsGoingUp() || IsGoingDown())
    {
      Serial.println("ERROR: Gate open while moving");
      EmergencyStop();
    }
  }

  if (ButtonStateEstop()){
    if (IsGoingUp() || IsGoingDown())
    {
      Serial.println("Emergency Stop Pressed");
      EmergencyStop();
    }
  }

  if (ButtonStateINPUT_ALARM())
  {
    SetBuzzerActive(true);
  }
  else
  {
    SetBuzzerActive(false);
  }
}

void CheckAlarm()
{
  if (ButtonStateINPUT_ALARM())
  {
    // if (currentMode == MODE_NORMAL)
    // {
    //   EmergencyStop();
    // }
    Serial.print(alarmDown);
    Serial.print(" ");
    Serial.println(millis() - alarmDownTime);
    if (!alarmDown)
    {
      Serial.println("CheckAlarm Down");
      if (currentMode != MODE_OVERRIDE)
      {
        // EmergencyStop();
      }
      alarmDown = true;
      alarmDownTime = millis();
    }
    else if (millis() - alarmDownTime > alarmOverrideInterval) // Switch to Override mode
    {
      if (currentMode == MODE_NORMAL)
      {
        SetCurrentMode(MODE_OVERRIDE);
        for (int i = 0; i < 3; i++)
        {
          SetBuzzerActive(true);
          delay(100);
          SetBuzzerActive(false);
          delay(100);
        }
      }
      else
      {
        SetCurrentMode(MODE_NORMAL);
        for (int i = 0; i < 4; i++)
        {
          SetBuzzerActive(true);
          delay(100);
          SetBuzzerActive(false);
          delay(100);
        }
      }
      SetLatchOpen(true);
      alarmDown = false;
    }
  }
  else
  {
    if (alarmDown)
    {
      Serial.println("Alarm Up");
      alarmDown = false;
    }
  }
}

bool ButtonStateEstop()
{
  temp = !mcp.digitalRead(MCP_INPUT_ESTOP);
  return temp ^= IsInvertedESTOP();
}

bool ButtonStateINPUT_UP()
{
  temp = !mcp.digitalRead(MCP_INPUT_UP);
  return temp ^= IsInvertedINPUT_UP();
}

bool ButtonStateINPUT_DOWN()
{
  temp = !mcp.digitalRead(MCP_INPUT_DOWN);
  return temp ^= IsInvertedINPUT_DOWN();
}

bool ButtonStateINPUT_ALARM()
{
  temp = !mcp.digitalRead(MCP_INPUT_ALARM);
  return temp ^= IsInvertedINPUT_ALARM();
}

bool ButtonStateLIFT_MIN()
{
  temp = !mcp.digitalRead(MCP_INPUT_LIFT_MIN);

  if (temp != liftMinState && liftMinChangeStart == -1){
    liftMinChangeStart = millis();
  } else if (temp != liftMinState && millis() - liftMinChangeStart > senseDelayFilter){
    liftMinState = temp;
  }

  if (temp == liftMinState){
    liftMinChangeStart = -1;
  }
  
  return liftMinState ^= IsInvertedLIFT_MIN();
}

bool ButtonStateLIFT_MAX()
{
  temp = !mcp.digitalRead(MCP_INPUT_LIFT_MAX);

  if (temp != liftMaxState && liftMaxChangeStart == -1){
    liftMaxChangeStart = millis();
  } else if (temp != liftMaxState && millis() - liftMaxChangeStart > senseDelayFilter){
    liftMaxState = temp;
  }

  if (temp == liftMaxState){
    liftMaxChangeStart = -1;
  }

  return liftMaxState ^= IsInvertedLIFT_MAX();
}

bool ButtonStateRAMP_MIN()
{
  temp = !mcp.digitalRead(MCP_INPUT_RAMP_MIN);

  if (temp != rampMinState && rampMinChangeStart == -1){
    rampMinChangeStart = millis();
  } else if (temp != rampMinState && millis() - rampMinChangeStart > senseDelayFilter){
    rampMinState = temp;
  }

  if (temp == rampMinState){
    rampMinChangeStart = -1;
  }

  return rampMinState ^= IsInvertedRAMP_MIN();
}

bool ButtonStateRAMP_MAX()
{
  temp = !mcp.digitalRead(MCP_INPUT_RAMP_MAX);

  if (temp != rampMaxState && rampMaxChangeStart == -1){
    rampMaxChangeStart = millis();
  } else if (temp != rampMaxState && millis() - rampMaxChangeStart > senseDelayFilter){
    rampMaxState = temp;
  }

  if (temp == rampMaxState){
    rampMaxChangeStart = -1;
  }

  return rampMaxState ^= IsInvertedRAMP_MAX();
}

bool ButtonStateGATE()
{
  temp = !mcp.digitalRead(MCP_INPUT_GATE);

  if (temp != gateSenseState && gateSenseChangeStart == -1){
    gateSenseChangeStart = millis();
  } else if (temp != gateSenseState && millis() - gateSenseChangeStart > senseDelayFilter){
    gateSenseState = temp;
  }

  if (temp == gateSenseState){
    gateSenseChangeStart = -1;
  }

  return gateSenseState ^= IsInvertedGATE();
}

void EmergencyStop()
{
  SetCurrentMode(MODE_NORMAL);
  Serial.println("EMERGENCY STOP INITIATED");
  ClearTasks(); // forces current task to stop and task buffer to clear;
}

void InputTest()
{
  Serial.print("rampMin: ");
  Serial.print(ButtonStateRAMP_MIN());
  Serial.print("\t");

  Serial.print("rampMax: ");
  Serial.print(ButtonStateRAMP_MAX());
  Serial.print("\t");

  Serial.print("liftMin: ");
  Serial.print(ButtonStateLIFT_MIN());
  Serial.print("\t");

  Serial.print("liftMax: ");
  Serial.print(ButtonStateLIFT_MAX());
  Serial.print("\t");

  Serial.print("gate: ");
  Serial.print(ButtonStateGATE());
  Serial.print("\t");

  Serial.print("up: ");
  Serial.print(ButtonStateINPUT_UP());
  Serial.print("\t");

  Serial.print("down: ");
  Serial.print(ButtonStateINPUT_DOWN());
  Serial.print("\t");

  Serial.print("estop: ");
  Serial.print(ButtonStateEstop());
  Serial.print("\t");

  Serial.print("alarm: ");
  Serial.print(ButtonStateINPUT_ALARM());
  Serial.print("\n");

  Serial.println();

  delay(200);
}

void SetBuzzerActive(bool isOn)
{
  if (isOn)
  {
    mcp.digitalWrite(MCP_PIN_BUZZER, HIGH);
  }
  else
  {
    mcp.digitalWrite(MCP_PIN_BUZZER, LOW);
  }
}

void SetLatchOpen(bool isOn)
{
  isOn ^= IsInvertedLATCH_OUTPUT();
  if (isOn)
  {
    Serial.println("Latch LOW");
    mcp.digitalWrite(MCP_PIN_LATCH, LOW);
  }
  else
  {
    Serial.println("Latch HIGH");
    mcp.digitalWrite(MCP_PIN_LATCH, HIGH);
  }
}

void SetTestMode(bool isOn)
{
  if (isOn)
  {
    SetCurrentMode(MODE_TEST);
  }
  else
  {
    SetCurrentMode(MODE_NORMAL);
  }
}

void SetIndicatorFlashCount(int count)
{
  flashCount = count;
  currentFlashCount = 0;
  digitalWrite(PIN_STATUS_LED, LOW);
}

void FlashIndicator()
{
  if (currentFlashCount < flashCount)
  {
    if (millis() - lastFlash > shortFlashInterval)
    {
      digitalWrite(PIN_STATUS_LED, !digitalRead(PIN_STATUS_LED));
      lastFlash = millis();

      if (!digitalRead(PIN_STATUS_LED))
      {
        currentFlashCount++;
      }
    }
  }
  else
  {
    if (millis() - lastFlash > longFlashInterval)
    {
      digitalWrite(PIN_STATUS_LED, !digitalRead(PIN_STATUS_LED));
      lastFlash = millis();
      currentFlashCount = 0;
    }
  }
}
