int accelIncrement = 1; // high number means faster acceleration

void SetMotorTarget(int idx, int tgt){
  if (idx == 1){
    Serial.print("Ramp Motor set to: ");
    Serial.println(tgt);
    motor1_target = tgt;
  } else if (idx == 2){
    Serial.print("Lift Motor set to: ");
    Serial.println(tgt);
    motor2_target = tgt;
  }
}

void UpdateMotorControl()
{

  if (motor1_target == 0)
  {
    SetMotorSpeed(1, 0);
  }
  else if (motor1_speed < motor1_target)
  {
    if (accelIncrement > 0)
    {
      SetMotorSpeed(1, motor1_speed + accelIncrement);
    }
    else
    {
      SetMotorSpeed(1, motor1_target);
    }
  }
  else if (motor1_speed > motor1_target)
  {    
    if (accelIncrement > 0)
    {
      SetMotorSpeed(1, motor1_speed - accelIncrement);
    }
    else
    {
      SetMotorSpeed(1, motor1_target);
    }
  }

  if (motor2_target == 0)
  {
    SetMotorSpeed(2, 0);
  }
  else if (motor2_speed < motor2_target)
  {
    
    if (accelIncrement > 0)
    {
      SetMotorSpeed(2, motor2_speed + accelIncrement);
    }
    else
    {
      SetMotorSpeed(2, motor2_target);
    }
  }
  else if (motor2_speed > motor2_target)
  {
    if (accelIncrement > 0)
    {
      SetMotorSpeed(2, motor2_speed - accelIncrement);
    }
    else
    {
      SetMotorSpeed(2, motor2_target);
    }
  }
}

void SetMotorSpeed(int motorID, int motorSpeed)
{
  if (motorSpeed > 255)
  {
    motorSpeed = 255;
  }
  else if (motorSpeed < -255)
  {
    motorSpeed = -255;
  }

  if (motorID == 1)
  {
    motor1_speed = motorSpeed;

    if (motorSpeed == 0)
    {
      mcp.digitalWrite(MOTOR1_INH_MCP, 0);
      digitalWrite(MOTOR1_IN1, 0);
      digitalWrite(MOTOR1_IN2, 0);
    }
    else if (motorSpeed > 0)
    {
      mcp.digitalWrite(MOTOR1_INH_MCP, 1);
      digitalWrite(MOTOR1_IN1, 0);      
      digitalWrite(MOTOR1_IN2, 1);
     
    }
    else if (motorSpeed < 0)
    {
      mcp.digitalWrite(MOTOR1_INH_MCP, 1);
      digitalWrite(MOTOR1_IN1, 1);      
      digitalWrite(MOTOR1_IN2, 0);
    }
  }
  else if (motorID == 2)
  {
    motor2_speed = motorSpeed;

    if (motorSpeed == 0)
    {
      mcp.digitalWrite(MOTOR2_INH_MCP, 0);
      digitalWrite(MOTOR2_IN1, 0);
      digitalWrite(MOTOR2_IN2, 0);
    }
    else if (motorSpeed > 0)
    {
      mcp.digitalWrite(MOTOR2_INH_MCP, 1);
      digitalWrite(MOTOR2_IN1, 0);      
      digitalWrite(MOTOR2_IN2, 1);      
    }
    else if (motorSpeed < 0)
    {
      mcp.digitalWrite(MOTOR2_INH_MCP, 1);
      digitalWrite(MOTOR2_IN1, 1);
      digitalWrite(MOTOR2_IN2, 0);
      
    }
  }
}


void Motor2Test()
{
  mcp.digitalWrite(MOTOR2_INH_MCP, HIGH);
  analogWrite(MOTOR2_IN1, 0);
  for (int i = 0; i < 255; i++)
  {
    analogWrite(MOTOR2_IN2, i);
    Serial.println(i);
    delay(10);
  }
  for (int i = 255; i > 0; i--)
  {
    analogWrite(MOTOR2_IN2, i);
    Serial.println(i);
    delay(10);
  }

  analogWrite(MOTOR2_IN2, 0);
  for (int i = 0; i < 255; i++)
  {
    analogWrite(MOTOR2_IN1, i);
    Serial.println(i);
    delay(10);
  }
  for (int i = 255; i > 0; i--)
  {
    analogWrite(MOTOR2_IN1, i);
    Serial.println(i);
    delay(10);
  }

  delay(1000);
  mcp.digitalWrite(MOTOR2_INH_MCP, LOW);
}

void Motor1Test()
{
  mcp.digitalWrite(MOTOR1_INH_MCP, HIGH);
  analogWrite(MOTOR1_IN1, 0);
  for (int i = 0; i < 255; i++)
  {
    analogWrite(MOTOR1_IN2, i);
    Serial.println(i);
    delay(10);
  }
  for (int i = 255; i > 0; i--)
  {
    analogWrite(MOTOR1_IN2, i);
    Serial.println(i);
    delay(10);
  }

  analogWrite(MOTOR1_IN2, 0);
  for (int i = 0; i < 255; i++)
  {
    analogWrite(MOTOR1_IN1, i);
    Serial.println(i);
    delay(10);
  }
  for (int i = 255; i > 0; i--)
  {
    analogWrite(MOTOR1_IN1, i);
    Serial.println(i);
    delay(10);
  }

  delay(1000);
  mcp.digitalWrite(MOTOR1_INH_MCP, LOW);
}

void Motor1SlowDrive(int max)
{
  mcp.digitalWrite(MOTOR1_INH_MCP, HIGH);
  analogWrite(MOTOR1_IN1, 0);
  for (int i = 0; i < max; i++)
  {
    analogWrite(MOTOR1_IN2, i);
    Serial.println(i);
    delay(10);
  }
}
