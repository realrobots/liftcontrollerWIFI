![image-20210120130520480](./MountingReference.jpg)

### UPDATE FIRMWARE

Enter WIFI mode by holding ALARM on boot.

If device cannot connect to local WIFI it will enter Access Point mode.

​	On a phone or PC connect to the WIFI access point RealRobotsLift

​	Use web browser to navigate to 192.168.1.4 (may need to turn off mobile data)

​	Enter correct Wifi credentials and the device will reboot.

​	Return to step 1

If device connects to WIFI it will check for updates, if one is found, 7 beeps will sound and the device will update.

If no update is found the device will beep 1 time.

The device will reset itself after updating (wait up to 1 minute) and will beep 4 times on restart.



### SERIAL DEBUGGING

Both NL & CR

115200

| Command | Description |
| --- | --- |
| $ | List Current Settings |
| $x=n | Overwrite setting index *x* with value *n* 0/1 |
| ? | Help Menu *normal function halted in help mode*|
| d | Revert to default settings |
| i | Input Test *lists current input states*|
| m | Lift Motor Test |
| n | Ramp Motor Test |
| l | Latch Test *Opens latch for 1s, then closes* |
| b | Buzzer Test *Sounds buzzer to 0.5s* |
| q | Return to normal mode |

### INDICTOR LIGHT
1 Flashes   Idle

2 Flashes   Moving Up

3 Flashes   Moving Down

5 Flashes   Emergency Stop Activated

7 Flashes   Override Mode
### INPUTS

##### BTN UP

Puts the lift into the "up" cycle.

1. Lift waits for gate sensor to be tripped by closed gate
2. Gate latch power cut, locking gate
3. Ramp motor turns clockwise until "Ramp Max" is triggered.
4. Lift motor turns clockwise until "Lift Max" is triggered.
5. Ramp motor turns counter clockwise until "Ramp Min" in triggered.
6. Gate latch power activated, unlocking gate

Pressing while already in cycle does nothing.

Pressing while in down cycle will cancel that cycle and begin an "up" cycle.



##### BTN DOWN

Puts the lift into the "down" cycle.

1. Lift waits for gate sensor to be tripped by closed gate
2. Gate latch power cut, locking gate
3. Ramp motor turns clockwise until "Ramp Max" is triggered.
4. Lift motor turns counter clockwise until "Lift Min" is triggered.
5. Ramp motor turns counter clockwise until "Ramp Min" in triggered.
6. Gate latch power activated, unlocking gate

Pressing while already in cycle does nothing.

Pressing while in up cycle will cancel that cycle and begin an "down" cycle.



##### BTN ESTOP

Immediately cancels the current cycle and power to motors. 



##### BTN ALARM

Activates loud buzzer while held.



##### GATE SENSOR

Limit switch for gate.



##### LIFT MAX

Uppermost limit switch for the lift actuator



##### LIFT MIN

Lowest limit switch for the lift actuator



##### RAMP MAX

Uppermost limit switch for the ramp actuator



##### RAMP MIN

Lowest limit switch for the ramp actuator



### OUTPUTS

##### LATCH POWER

Drives 24v to latch actuator.

##### LIFT MOTOR

24v to lift actuator

##### RAMP MOTOR

24v to lift actuator


### OVERRIDE MODE

Allows direct control of motors. 
Automatically releases gate latch.
UP/DOWN buttons move lift motor.
Hold ESTOP and press UP/DOWN buttons to move ramp.

To initiate OVERRIDE MODE Hold the ALARM for 10 seconds.
3 quick beeps will sound to indicate override mode.

Hold ALARM for 10 seconds (without pushing any other buttons) to go back to NORMAL mode.
4 quick beeps will sound to indicate return to normal mode.



### FIRMWARE

Firmware can be updated using a provided USB->UART converter plugged into the "UART Programmer port" and using the Arduino IDE. Set the board to "Arduino Uno".

Latest firmware can be downloaded at https://gitlab.com/jakelwilkinson/liftcontroller/